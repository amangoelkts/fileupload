package com.example.fileuploadproject.controller;


import com.example.fileuploadproject.service.FileUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
//committed again in intellij after pull request merged
@RestController
public class FileUploadController {


    //now edit after cloning

    @Autowired
    FileUploadService fileUploadService;

    @PostMapping("/uploadFile")
    public void uploadFile(@RequestParam("file")MultipartFile file) throws IOException {
        fileUploadService.uploadFile(file);

    }




}
